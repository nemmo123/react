import React from "react";
import useCustomCounter from "./CustomHook1";
const CustomHook = () =>{
    const data = useCustomCounter()
    const data1 = useCustomCounter()
    return(
        <>
        <h1>CustomHook</h1>
        <h1>count:{data.count}</h1>
       <button type="button" onClick={data.handleIncrement}>add </button>
       <h1>count:{data1.count}</h1>
       <button type="button" onClick={data1.handleIncrement}>add</button>
       </>
    )
}
export default CustomHook