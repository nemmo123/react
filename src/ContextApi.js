
import React, { createContext} from "react"
import Component1 from "./Component1";
const context = createContext()
// eslint-disable-next-line react-hooks/rules-of-hooks
const ContextApi = () => {
  return(
  <>
   <context.Provider value={'done'}> <Component1 /></context.Provider>
   </>
  ) 
}

export default ContextApi
export {context}
