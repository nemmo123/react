import {useState} from "react"
const useCustomCounter = () => { 

    const[count, setCounter] = useState(0);
    const handleIncrement = () => {
      setCounter(count + 1)
      
    }
    return {
    
        count,
        handleIncrement
        
    }
   
}
export default useCustomCounter