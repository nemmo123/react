import changeValue from "./index";
import { combineReducers } from "redux";
const rootReducer = combineReducers({
    changeValue
})
export default rootReducer