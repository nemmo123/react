import {useState, useMemo} from 'react'
const Memo =()=>{
    const[state, setState] = useState(0)
    // eslint-disable-next-line no-undef
    const useMemoCounter = useMemo(()=>{
        return (
            <div> <h1>useMemo {state}</h1>
            <p>it uses for rendering specific term. this term will not rendering.
                
            </p></div>
        )
       
    },[])
    return(
        <>
       {useMemoCounter}
        <p> memocount:{state}</p>
        <button onClick={()=>setState(state+1)}> inc</button>
        </>
    )
}
export default Memo