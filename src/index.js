import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import ContextApi from './ContextApi'
import reportWebVitals from './reportWebVitals';
import store from "./store"
import { Provider } from "react-redux"
import CustomHook from './CustomHook';
import Memo from './Memo'
import AxiosExample from './Axios';
store.subscribe(() => console.log(store.getState()))
ReactDOM.render(
  <React.StrictMode>
    <Provider store= {store}>
      {/* <App /> */}
      {/* <CustomHook /> */}
      {/* <ContextApi /> */}
      {/* <Memo/> */}
      <AxiosExample />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
