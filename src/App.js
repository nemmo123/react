
import './App.css';
import React, { createContext} from "react"
// import Component1 from "./Component1";
import {addInc, subDec} from './store1/actions/index.js'
import { useSelector, useDispatch } from 'react-redux';
// const context = createContext()
const App=()=> {
  const myState = useSelector((state)=> state.changeValue)
  const dispatch =useDispatch()
  return (
    <>
    <div>
      <h1>Redux Example</h1>
      <p>it manage state</p>
      <p>redux</p>
    </div>
   <div><span>
  <button onClick={()=>dispatch(addInc())}>+</button>
    </span>
     <span>
     <button onClick={()=>dispatch(subDec())}>-</button>{myState}</span>
    </div>
    {/* <context.Provider value={'done'}> <Component1 /></context.Provider> */}
    </>
   )
 }

export default App;
// export {context}
