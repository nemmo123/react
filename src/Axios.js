import axios from "axios";
import { useState, useEffect } from "react";

const AxiosExample = () => {
    const[data, setData]=useState('')
    useEffect(()=>{
        axios.get("http://jsonplaceholder.typicode.com/posts")
        .then((response)=>{
            setData(response.data.title)
        })
        .catch(error => console.error(`Error:${error}`))
    },[])

    return(
        <ul className="posts">
        {data.map((post) => (
          <li className="post" key={post.id}>
            <h4>{post.title}</h4>
            <p>{post.body}</p>
          </li>
        ))}
      </ul>
    )
}

export default AxiosExample



















