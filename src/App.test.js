import { render, screen } from '@testing-library/react';
// import App from './App';
import CustomHook from './CustomHook'

test('renders learn react link', () => {
  render(
  // <App />
  <CustomHook />
  );
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
